# RSVV 2024 Website

The RSVV.nl website is a rebuild, redesigned WordPress theme developed by [Robin Corba](https://www.robincorba.com) in February 2024.

---

## Update instructions

To update this project update the versions in the following files:


- ./Dockerfile
- ./docker-compose.yml (don't forget to run `mysql_upgrade -uroot -p<rootuserpassword>` after you update the MariaDB version)
- /src/composer.json
- /src/web/app/themes/rsvv-2024/package.json

---

## Installation instructions

### Prerequisites

Make sure you have Docker Desktop (v20) installed on your computer.

### How to set up the development environment

1. Copy the `.env.example` to `.env` and fill in;
1. Run `docker compose up` from the root folder to start all containers
1. To compile a new build of the WordPress theme;
    - Access the website container with `docker exec -it rsvv-website /bin/bash`
    - Go to `web/app/themes/rsvv-2024/` and run `yarn run build` (Note: `yarn run dev` does NOT compile styling for editor)
1. Access the CMS on http://localhost/wp-admin with user `admin` with password `admin`
1. Access the website on http://localhost. Happy coding!

---

### Notes

- Access PHPMyAdmin on http://localhost:8080
- Acces the mailhog inbox simulator on http://localhost:8025

---

# Add translations

Run the following commands inside the `rsvv-website` docker container, inside the `/var/www/html/web/app/themes/rsvv-2024` folder:

Generate a new template file:
`wp i18n make-pot . rsvv.pot --domain=rsvv --exclude=theme.json --allow-root`

Create a copy and make it a `nl_NL.po` file, fill the translations, then create a .mo file:
`msgfmt -o nl_NL.mo nl_NL.po`

---

## Deployment instructions

COMING SOON
