# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Don't forget to also update the version in the following files for each update;
- `src/web/app/themes/rsvv-2024/package.json`
- `src/web/app/themes/rsvv-2024/style.css`

---
[0.1.9] [2024-11-10] Added OpenStreetMap block
[0.1.8] [2024-11-04] Moved all sports related post types under the "Sportaanbod" menu item
[0.1.7] [2024-11-04] Changed Target Audience from taxonomy to post type
[0.1.6] [2024-11-04] Removed obsolete version attribute from docker-compose.yml
[0.1.5] [2024-10-06] Added footer
[0.1.4] [2024-08-30] Added custom block News Cards
[0.1.3] [2024-08-21] Added full width background style to WP Block Group
[0.1.2] [2024-08-21] Added white color to editor palette
[0.1.1] [2024-08-19] Added max width to root container in editor
[0.1.0] [2024-08-12] Added custom block RSVV Card
[0.0.9] [2024-07-09] Made site header fixed
[0.0.8] [2024-07-07] Added styling for homepage header (and RSVV logo)
[0.0.7] [2024-07-05] Added styling for button
[0.0.6] [2024-05-31] Added styling for WP Block Group
[0.0.5] [2024-05-31] Added paragraph block style RSVV underline
[0.0.4] [2024-05-24] Added Snyk configuration to VS Code workspace
[0.0.3] [2024-05-18] Added styling for WP Block Media & Text
[0.0.2] [2024-04-12] Added language switcher to navigation
[0.0.1] [2024-02-23] Initial WordPress Bedrock + Sage starter theme setup for RSVV
