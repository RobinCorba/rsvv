/**
 * Compiler configuration
 *
 * @see {@link https://roots.io/sage/docs sage documentation}
 * @see {@link https://bud.js.org/learn/config bud.js configuration guide}
 *
 * @type {import('@roots/bud').Config}
 */
export default async (app) => {
  /**
   * Application assets & entrypoints
   *
   * @see {@link https://bud.js.org/reference/bud.entry}
   * @see {@link https://bud.js.org/reference/bud.assets}
   */
  app
    .entry('app', ['@scripts/app', '@styles/app'])
    .entry('editor', ['@scripts/editor', '@styles/editor'])
    .assets(['fonts', 'images']);

  /**
   * Set public path
   *
   * @see {@link https://bud.js.org/reference/bud.setPublicPath}
   */
  app.setPublicPath('/app/themes/rsvv-2024/public/');

  /**
   * Development server settings
   *
   * @see {@link https://bud.js.org/reference/bud.setUrl}
   * @see {@link https://bud.js.org/reference/bud.setProxyUrl}
   * @see {@link https://bud.js.org/reference/bud.watch}
   */
  app
    .setUrl('http://localhost:3000')
    .setProxyUrl('http://rsvv.test')
    .watch(['resources/views', 'app']);

  /**
   * Generate WordPress `theme.json`
   *
   * @note This overwrites `theme.json` on every build.
   *
   * @see {@link https://bud.js.org/extensions/sage/theme.json}
   * @see {@link https://developer.wordpress.org/block-editor/how-to-guides/themes/theme-json}
   */
  app.wpjson.enable();
  app.wpjson
    .setSettings({
      background: {
        backgroundImage: true,
      },
      color: {
        custom: false,
        customDuotone: false,
        customGradient: false,
        defaultDuotone: false,
        defaultGradients: false,
        defaultPalette: false,
        duotone: [],
        palette: [
          {
            color: '#3564AC',
            name: 'RSVV Blue',
            slug: 'rsvv-blue',
          },
          {
            color: '#D8E3F3',
            name: 'RSVV Light Blue',
            slug: 'rsvv-light-blue',
          },
          {
            color: '#000',
            name: 'Black',
            slug: 'black',
          },
          {
            color: '#FFFFFF',
            name: 'White',
            slug: 'white',
          },
        ],
      },
      custom: {
        spacing: {},
        typography: {
          'font-size': {},
          'line-height': {},
        },
      },
      spacing: {
        padding: true,
        units: ['px', '%', 'em', 'rem', 'vw', 'vh'],
        spacingSizes: [
          {
            name: '1',
            slug: '20',
            size: '1.5rem',
          },
          {
            name: '2',
            slug: '30',
            size: '3rem',
          },
          {
            name: '3',
            slug: '40',
            size: '4.5rem',
          },
        ],
      },
      typography: {
        customFontSize: false,
        fontFamilies: [
          {
            fontFamily: '"Open sans", sans-serif',
            name: 'Open sans',
            slug: 'open-sans',
            fontFace: [
              {
                fontFamily: 'Open sans',
                fontStretch: 'normal',
                fontStyle: 'normal',
                fontWeight: '400',
                src: [
                  'file:./resources/fonts/open-sans.ttf',
                ],
              },
            ],
          },
        ],
      },
    });
};
