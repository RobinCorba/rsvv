<?php

namespace App\Models;

use RuntimeException;
use stdClass;
use WP_Post;

class Activity
{
  public int $id;
  public bool $status;
  public string $day;
  public string $time_start;
  public string $time_end;
  public object $trainer;
  public object $sport;
  public object $audience;
  public object $location;

  function __construct(WP_Post $object)
  {
    $this->populateFromPost($object);
  }

  /**
   * Populates the Activity model from a WordPress post object
   *
   * @param WP_Post $object WordPress post object
   * @throws RuntimeException If required meta fields are missing
   */
  private function populateFromPost(WP_Post $object): void
  {
    $this->id = $object->ID;
    $fields = get_post_meta($object->ID);

    // Set basic properties
    $this->setBasicProperties($fields);

    // Set related entities
    $this->audience = $this->createRelatedEntity('audience', $fields['acitvity_audience'][0] ?? null);
    $this->location = $this->createRelatedEntity('location', $fields['location'][0] ?? null);
    $this->sport = $this->createRelatedEntity('sport', $fields['activity_sport'][0] ?? null);
    $this->trainer = $this->createRelatedEntity('trainer', $fields['trainer'][0] ?? null);
  }

  /**
   * Sets the basic properties of the activity
   *
   * @param array $fields Post meta fields
   * @throws RuntimeException If required fields are missing
   */
  private function setBasicProperties(array $fields): void
  {
    $requiredFields = [
      'status' => 'status',
      'date-and-time_day' => 'day',
      'date-and-time_time-start' => 'time_start',
      'date-and-time_time-end' => 'time_end',
    ];

    foreach ($requiredFields as $field => $property) {
      if (!isset($fields[$field][0])) {
        throw new RuntimeException("Required field '{$field}' is missing");
      }
      $this->$property = $fields[$field][0];
    }
  }

  /**
   * Creates a related entity object with id and name
   *
   * @param string $type Entity type (audience, location, sport or trainer)
   * @param string|null $id Entity ID
   * @return stdClass|null
   * @throws RuntimeException If entity meta data is missing
   */
  private function createRelatedEntity(string $type, ?string $id): ?stdClass
  {
    if (empty($id)) {
      return null;
    }

    $entity = new stdClass();
    $entity->id = $id;

    $fields = get_post($id);
    $nameField = "post_title";

    if (!isset($fields->$nameField)) {
      throw new RuntimeException("Required field '{$nameField}' is missing for {$type} with ID: {$id}");
    }
    $entity->name = $fields->$nameField;

    return $entity;
  }
}
