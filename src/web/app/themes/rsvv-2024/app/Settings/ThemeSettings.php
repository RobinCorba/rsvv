<?php

namespace App\Settings;

use WP_Error;

class ThemeSettings
{

  /**
   * Registers custom settings of the theme to WordPress
   *
   * @return bool Returns true if the options page and fields are registered
   */
  public static function register(): bool
  {
    if (!function_exists('acf_add_local_field_group')) {
      return false;
    }

    $result = acf_add_local_field_group(array(
      'key' => 'group_theme_settings',
      'title' => 'Footer settings',
      'fields' => [
        [
          'key' => 'field_footer_slogan',
          'label' => 'Slogan',
          'name' => 'footer_slogan',
          'instructions' => 'The sentence below is displayed in the footer, below the site title',
          'type' => 'text',
          'wrapper' => [
            'width' => '100%',
            'class' => '',
            'id' => '',
          ],
          'maxlength' => '120',
          'placeholder' => 'Sport wordt beoefend met een recreatief karakter, voor jong en oud!',
        ],
        [
          'key' => 'field_socmed_facebook_url',
          'label' => 'Facebook URL',
          'name' => 'socmed_facebook_url',
          'type' => 'text',
          'wrapper' => [
            'width' => '50%',
            'class' => '',
            'id' => '',
          ],
        ],
        [
          'key' => 'field_socmed_instagram_url',
          'label' => 'Instagram URL',
          'name' => 'socmed_instagram_url',
          'type' => 'text',
          'wrapper' => [
            'width' => '50%',
            'class' => '',
            'id' => '',
          ],
        ],
        [
          'key' => 'field_contact_address',
          'label' => 'Adress',
          'name' => 'contact_address',
          'type' => 'textarea',
          'new_lines' => 'br',
          'wrapper' => [
            'width' => '50%',
            'class' => '',
            'id' => '',
          ],
        ],
        [
          'key' => 'field_contact_email',
          'label' => 'E-mail',
          'name' => 'contact_email',
          'type' => 'email',
          'wrapper' => [
            'width' => '50%',
            'class' => '',
            'id' => '',
          ],
        ],
        [
          'key' => 'field_contact_phone_number',
          'label' => 'Phone number',
          'name' => 'contact_phone_number',
          'type' => 'text',
          'wrapper' => [
            'width' => '50%',
            'class' => '',
            'id' => '',
          ],
        ],
        [
          'key' => 'field_contact_phone_number_note',
          'label' => 'Phone number note',
          'name' => 'contact_phone_number_note',
          'type' => 'textarea',
          'new_lines' => 'br',
          'wrapper' => [
            'width' => '50%',
            'class' => '',
            'id' => '',
          ],
        ],
      ],
      'location' => [
        [
          [
            'param' => 'options_page',
            'operator' => '==',
            'value' => 'theme-settings',
          ],
        ],
      ],
      'menu_order' => 0,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
      'active' => true,
      'description' => '',
      'show_in_rest' => 0,
    ));

    if ($result instanceof WP_Error) {
      return false;
    }

    return true;
  }
}
