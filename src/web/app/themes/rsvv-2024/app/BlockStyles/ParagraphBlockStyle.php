<?php

namespace App\BlockStyles;

class ParagraphBlockStyle
{

  /**
   * Registers a custom block style to the existing Paragraph block
   *
   * @return bool Returns true if block style is registered
   */
  public static function register(): bool
  {
    if (!function_exists('register_block_style')) {
      return false;
    }

    register_block_style('core/paragraph', [
      'name' => 'rsvv-underline',
      'label' => __('RSVV Underline', 'rsvv')
    ]);

    return true;
  }
}
