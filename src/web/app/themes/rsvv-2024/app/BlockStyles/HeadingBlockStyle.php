<?php

namespace App\BlockStyles;

class HeadingBlockStyle
{

  /**
   * Registers a custom block style to the existing Heading block
   *
   * @return bool Returns true if block style is registered
   */
  public static function register(): bool
  {
    if (!function_exists('register_block_style')) {
      return false;
    }

    register_block_style('core/heading', [
      'name' => 'rsvv-underline',
      'label' => __('RSVV Underline', 'rsvv')
    ]);

    return true;
  }
}
