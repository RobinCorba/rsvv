<?php

namespace App\BlockStyles;

class ButtonBlockStyle
{

  /**
   * Registers a custom block style to the existing Button block
   *
   * @return bool Returns true if block style is registered
   */
  public static function register(): bool
  {
    if (!function_exists('register_block_style')) {
      return false;
    }

    register_block_style('core/button', [
      'name' => 'rsvv-button',
      'label' => __('RSVV Button', 'rsvv')
    ]);

    return true;
  }
}
