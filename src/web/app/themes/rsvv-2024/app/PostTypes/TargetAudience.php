<?php

namespace App\PostTypes;

use WP_Error;

class TargetAudience
{

  /**
   * Registers the custom post type to WordPress
   *
   * @return bool Returns true if post type is registered
   */
  public static function register(): bool
  {
    $result = register_post_type(
      'target-audience',
      [
        'labels' => [
          'name' => 'Doelgroepen',
          'singular_name' => 'Doelgroep',
          'menu_name' => 'Doelgroepen',
          'all_items' => 'Doelgroepen',
          'edit_item' => 'Doelgroep bewerken',
          'view_item' => 'Doelgroep bekijken',
          'view_items' => 'Doelgroepen bekijken',
          'add_new_item' => 'Nieuwe doelgroep toevoegen',
          'add_new' => 'Nieuwe doelgroep',
          'new_item' => 'Nieuwe doelgroep',
          'parent_item_colon' => 'Hoofddoelgroep:',
          'search_items' => 'Doelgroepen zoeken',
          'not_found' => 'Geen doelgroepen gevonden',
          'not_found_in_trash' => 'Geen doelgroepen gevonden in de prullenbak',
          'archives' => 'Doelgroep archieven',
          'attributes' => 'Doelgroep attributen',
          'insert_into_item' => 'Invoegen in doelgroep',
          'uploaded_to_this_item' => 'Geüpload naar deze doelgroep',
          'filter_items_list' => 'Filter doelgroepen lijst',
          'filter_by_date' => 'Filter doelgroepen op datum',
          'items_list_navigation' => 'Doelgroepenlijst navigatie',
          'items_list' => 'Doelgroepenlijst',
          'item_published' => 'Doelgroep gepubliceerd.',
          'item_published_privately' => 'Doelgroep privé gepubliceerd.',
          'item_reverted_to_draft' => 'Doelgroep teruggezet naar het concept.',
          'item_scheduled' => 'Doelgroep gepland.',
          'item_updated' => 'Doelgroep geüpdatet.',
          'item_link' => 'Doelgroep link',
          'item_link_description' => 'Een link naar een doelgroep.',
        ],
        'public' => true,
        'publicly_queryable' => false,
        'show_in_rest' => true,
        'show_in_menu' => 'edit.php?post_type=sport',
        'menu_icon' => 'dashicons-businessperson',
        'supports' => [
          0 => 'title',
        ],
        'delete_with_user' => false,
      ]
    );

    if ($result instanceof WP_Error) {
      return false;
    }

    return true;
  }
}
