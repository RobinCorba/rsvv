<?php

namespace App\PostTypes;

use WP_Error;

class Trainer
{

  /**
   * Registers the custom post type to WordPress
   *
   * @return bool Returns true if post type is registered
   */
  public static function register(): bool
  {
    $result = register_post_type(
      'trainer',
      [
        'labels' => [
          'name' => 'Trainers',
          'singular_name' => 'Trainer',
          'menu_name' => 'Trainers',
          'all_items' => 'Trainers',
          'edit_item' => 'Trainer bewerken',
          'view_item' => 'Trainer bekijken',
          'view_items' => 'Trainers bekijken',
          'add_new_item' => 'Nieuwe trainer toevoegen',
          'add_new' => 'Nieuwe trainer',
          'new_item' => 'Nieuwe trainer',
          'parent_item_colon' => 'Hoofdtrainer:',
          'search_items' => 'Trainers zoeken',
          'not_found' => 'Geen trainers gevonden',
          'not_found_in_trash' => 'Geen trainers gevonden in de prullenbak',
          'archives' => 'Trainer archieven',
          'attributes' => 'Trainer attributen',
          'insert_into_item' => 'Invoegen in trainer',
          'uploaded_to_this_item' => 'Geüpload naar deze trainer',
          'filter_items_list' => 'Filter trainers lijst',
          'filter_by_date' => 'Filter trainers op datum',
          'items_list_navigation' => 'Trainerslijst navigatie',
          'items_list' => 'Trainerslijst',
          'item_published' => 'Trainer gepubliceerd.',
          'item_published_privately' => 'Trainer privé gepubliceerd.',
          'item_reverted_to_draft' => 'Trainer teruggezet naar het concept.',
          'item_scheduled' => 'Trainer gepland.',
          'item_updated' => 'Trainer geüpdatet.',
          'item_link' => 'Trainer link',
          'item_link_description' => 'Een link naar een trainer.',
        ],
        'public' => true,
        'publicly_queryable' => false,
        'show_in_menu' => 'edit.php?post_type=sport',
        'show_in_rest' => true,
        'menu_icon' => 'dashicons-businessperson',
        'supports' => [
          0 => 'title',
        ],
        'delete_with_user' => false,
      ]
    );

    if ($result instanceof WP_Error) {
      return false;
    }

    return true;
  }
}
