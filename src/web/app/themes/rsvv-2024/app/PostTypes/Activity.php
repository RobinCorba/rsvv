<?php

namespace App\PostTypes;

use WP_Error;

class Activity
{

  /**
   * Registers the custom post type to WordPress
   *
   * @return bool Returns true if post type is registered
   */
  public static function register(): bool
  {
    if (!function_exists('acf_add_local_field_group')) {
      return false;
    }

    $result = register_post_type(
      'activity',
      [
        'labels' => [
          'name' => 'Rooster',
          'singular_name' => 'Sportactiviteit',
          'menu_name' => 'Rooster',
          'all_items' => 'Rooster',
          'edit_item' => 'Activiteit bewerken',
          'view_item' => 'Activiteit bekijken',
          'view_items' => 'Activiteiten bekijken',
          'add_new_item' => 'Nieuwe sportactiviteit',
          'add_new' => 'Nieuwe activiteit',
          'new_item' => 'Nieuw activiteit',
          'parent_item_colon' => 'Hoofdactiviteit:',
          'search_items' => 'Activiteit zoeken',
          'not_found' => 'Geen activiteiten gevonden',
          'not_found_in_trash' => 'Geen activiteiten gevonden in de prullenbak',
          'archives' => 'Sportactiviteit archieven',
          'attributes' => 'Activiteit attributen',
          'insert_into_item' => 'Invoegen in activiteit',
          'uploaded_to_this_item' => 'Geüpload naar deze activiteit',
          'filter_items_list' => 'Filter activiteitenlijst',
          'filter_by_date' => 'Filter activiteiten op datum',
          'items_list_navigation' => 'Activiteitlijst navigatie',
          'items_list' => 'Activiteitenlijst',
          'item_published' => 'Activiteit gepubliceerd.',
          'item_published_privately' => 'Activiteit privé gepubliceerd.',
          'item_reverted_to_draft' => 'Activiteit teruggezet naar het concept.',
          'item_scheduled' => 'Activiteit gepland.',
          'item_updated' => 'Activiteit geüpdatet.',
          'item_link' => 'Activiteit link',
          'item_link_description' => 'Een link naar een sportactiviteit.',
        ],
        'description' => 'Een sportactiviteit of -les in het rooster.',
        'public' => true,
        'publicly_queryable' => false,
        'show_in_menu' => 'edit.php?post_type=sport',
        'show_in_rest' => false,
        'menu_icon' => 'dashicons-awards',
        'supports' => [
          0 => 'title',
        ],
        'delete_with_user' => false,
      ]
    );

    if ($result instanceof WP_Error) {
      return false;
    }

    acf_add_local_field_group(
      [
        'key' => 'group_65d87e9628efb',
        'title' => 'Details van sportactiviteit',
        'fields' => [
          [
            'key' => 'field_activity_status',
            'label' => 'Status',
            'name' => 'status',
            'aria-label' => '',
            'type' => 'radio',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => [
              'width' => '50',
              'class' => '',
              'id' => '',
            ],
            'choices' => [
              'open' => 'Open (inschrijven mogelijk)',
              'full' => 'Vol (inschrijven niet mogelijk)',
            ],
            'default_value' => 'open',
            'return_format' => 'value',
            'allow_null' => 0,
            'other_choice' => 0,
            'layout' => 'vertical',
            'save_other_choice' => 0,
          ],
          [
            'key' => 'field_activity_date_and_time',
            'label' => 'Wanneer',
            'name' => 'date-and-time',
            'aria-label' => '',
            'type' => 'group',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => [
              'width' => '50',
              'class' => '',
              'id' => '',
            ],
            'layout' => 'table',
            'sub_fields' => [
              [
                'key' => 'field_activity_day',
                'label' => 'Dag van de week',
                'name' => 'day',
                'aria-label' => '',
                'type' => 'select',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => [
                  'width' => '50',
                  'class' => '',
                  'id' => '',
                ],
                'choices' => [
                  '1monday' => 'Maandag',
                  '2tuesday' => 'Dinsdag',
                  '3wednesday' => 'Woensdag',
                  '4thursday' => 'Donderdag',
                  '5friday' => 'Vrijdag',
                  '6saturday' => 'Zaterdag',
                  '7sunday' => 'Zondag',
                ],
                'default_value' => false,
                'return_format' => 'value',
                'multiple' => 0,
                'allow_null' => 0,
                'ui' => 0,
                'ajax' => 0,
                'placeholder' => '',
              ],
              [
                'key' => 'field_activity_time_start',
                'label' => 'Begintijd',
                'name' => 'time-start',
                'aria-label' => '',
                'type' => 'time_picker',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => [
                  'width' => '',
                  'class' => '',
                  'id' => '',
                ],
                'display_format' => 'H:i',
                'return_format' => 'H:i',
              ],
              [
                'key' => 'field_activity_time_end',
                'label' => 'Eindtijd',
                'name' => 'time-end',
                'aria-label' => '',
                'type' => 'time_picker',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => [
                  'width' => '',
                  'class' => '',
                  'id' => '',
                ],
                'display_format' => 'H:i',
                'return_format' => 'H:i',
              ],
            ],
          ],
          [
            'key' => 'field_activity_sport',
            'label' => 'Sport',
            'name' => 'activity_sport',
            'aria-label' => '',
            'type' => 'post_object',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => [
              'width' => '50',
              'class' => '',
              'id' => '',
            ],
            'post_type' => [
              0 => 'sport',
            ],
            'post_status' => [
              0 => 'publish',
            ],
            'taxonomy' => '',
            'return_format' => 'object',
            'multiple' => 0,
            'allow_null' => 0,
            'bidirectional' => 0,
            'ui' => 1,
            'bidirectional_target' => [
            ],
          ],
          [
            'key' => 'field_activity_location',
            'label' => 'Locatie',
            'name' => 'location',
            'aria-label' => '',
            'type' => 'post_object',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => [
              'width' => '50',
              'class' => '',
              'id' => '',
            ],
            'post_type' => [
              0 => 'location',
            ],
            'post_status' => [
              0 => 'publish',
            ],
            'taxonomy' => '',
            'return_format' => 'object',
            'multiple' => 0,
            'allow_null' => 0,
            'bidirectional' => 0,
            'ui' => 1,
            'bidirectional_target' => [
            ],
          ],
          [
            'key' => 'field_acitvity_audience',
            'label' => 'Doelgroep',
            'name' => 'acitvity_audience',
            'aria-label' => '',
            'type' => 'post_object',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => [
              'width' => '50',
              'class' => '',
              'id' => '',
            ],
            'post_type' => [
              0 => 'target-audience',
            ],
            'post_status' => [
              0 => 'publish',
            ],
            'taxonomy' => '',
            'return_format' => 'object',
            'multiple' => 0,
            'allow_null' => 0,
            'bidirectional' => 0,
            'ui' => 1,
            'bidirectional_target' => [],
          ],
          [
            'key' => 'field_activity_trainer',
            'label' => 'Trainer',
            'name' => 'trainer',
            'aria-label' => '',
            'type' => 'post_object',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => [
              'width' => '50',
              'class' => '',
              'id' => '',
            ],
            'post_type' => [
              0 => 'trainer',
            ],
            'post_status' => [
              0 => 'publish',
            ],
            'taxonomy' => '',
            'return_format' => 'object',
            'multiple' => 0,
            'allow_null' => 0,
            'bidirectional' => 0,
            'ui' => 1,
            'bidirectional_target' => [
            ],
          ],
        ],
        'location' => [
          [
            [
              'param' => 'post_type',
              'operator' => '==',
              'value' => 'activity',
            ],
          ],
        ],
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
        'show_in_rest' => 0,
      ]
    );

    return true;
  }
}
