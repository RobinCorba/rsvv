<?php

namespace App\PostTypes;

use WP_Error;

class Location
{

  /**
   * Registers the custom post type to WordPress
   *
   * @return bool Returns true if post type is registered
   */
  public static function register(): bool
  {
    if (!function_exists('acf_add_local_field_group')) {
      return false;
    }

    $result = register_post_type(
      'location',
      [
        'labels' => [
          'name' => 'Locaties',
          'singular_name' => 'Locatie',
          'menu_name' => 'Locaties',
          'all_items' => 'Locaties',
          'edit_item' => 'Locatie bewerken',
          'view_item' => 'Locatie bekijken',
          'view_items' => 'Locaties bekijken',
          'add_new_item' => 'Nieuwe locatie toevoegen',
          'add_new' => 'Nieuwe locatie',
          'new_item' => 'Nieuw locatie',
          'parent_item_colon' => 'Hoofdlocatie:',
          'search_items' => 'Locaties zoeken',
          'not_found' => 'Geen locaties gevonden',
          'not_found_in_trash' => 'Geen locaties gevonden in de prullenbak',
          'archives' => 'Locatie archieven',
          'attributes' => 'Locatie attributen',
          'insert_into_item' => 'Invoegen in locatie',
          'uploaded_to_this_item' => 'Geüpload naar deze locatie',
          'filter_items_list' => 'Filter locaties lijst',
          'filter_by_date' => 'Filter locaties op datum',
          'items_list_navigation' => 'Locaties lijst navigatie',
          'items_list' => 'Locaties lijst',
          'item_published' => 'Locatie gepubliceerd.',
          'item_published_privately' => 'Locatie privé gepubliceerd.',
          'item_reverted_to_draft' => 'Locatie teruggezet naar het concept.',
          'item_scheduled' => 'Locatie gepland.',
          'item_updated' => 'Locatie geüpdatet.',
          'item_link' => 'Locatie link',
          'item_link_description' => 'Een link naar een locatie.',
        ],
        'public' => true,
        'show_in_menu' => 'edit.php?post_type=sport',
        'show_in_rest' => true,
        'menu_icon' => 'dashicons-admin-post',
        'supports' => [
          0 => 'title',
          1 => 'thumbnail',
        ],
        'delete_with_user' => false,
      ]
    );

    if ($result instanceof WP_Error) {
      return false;
    }

    acf_add_local_field_group(
      [
        'key' => 'group_65d889f817b70',
        'title' => 'Details van locatie',
        'fields' => [
          [
            'key' => 'field_65d889f8b419f',
            'label' => 'Coördinaten',
            'name' => 'coordinates',
            'aria-label' => '',
            'type' => 'google_map',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => [
              'width' => '60',
              'class' => '',
              'id' => '',
            ],
            'center_lat' => '',
            'center_lng' => '',
            'zoom' => '',
            'height' => '',
          ],
          [
            'key' => 'field_65d8d6b8b3be6',
            'label' => 'Adres',
            'name' => 'address',
            'aria-label' => '',
            'type' => 'textarea',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => [
              'width' => '40',
              'class' => '',
              'id' => '',
            ],
            'default_value' => '',
            'maxlength' => '',
            'rows' => '',
            'placeholder' => '',
            'new_lines' => '',
          ],
        ],
        'location' => [
          [
            [
              'param' => 'post_type',
              'operator' => '==',
              'value' => 'location',
            ],
          ],
        ],
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
        'show_in_rest' => 0,
      ]
    );

    return true;
  }
}
