<?php

namespace App\PostTypes;

use WP_Error;

class Sport
{

  /**
   * Registers the custom post type to WordPress
   *
   * @return bool Returns true if post type is registered
   */
  public static function register(): bool
  {
    $result = register_post_type(
      'sport',
      [
        'labels' => [
          'name' => 'Sportaanbod',
          'singular_name' => 'Sport',
          'menu_name' => 'Sportaanbod',
          'all_items' => 'Sportaanbod',
          'edit_item' => 'Sport bewerken',
          'view_item' => 'Sport bekijken',
          'view_items' => 'Sportaanbod bekijken',
          'add_new_item' => 'Nieuwe sport toevoegen',
          'add_new' => 'Nieuwe sport',
          'new_item' => 'Nieuwe sport',
          'parent_item_colon' => 'Hoofdsport:',
          'search_items' => 'Sportaanbod zoeken',
          'not_found' => 'Geen sportaanbod gevonden',
          'not_found_in_trash' => 'Geen sportaanbod gevonden in de prullenbak',
          'archives' => 'Sport archieven',
          'attributes' => 'Sport attributen',
          'insert_into_item' => 'Invoegen in sport',
          'uploaded_to_this_item' => 'Geüpload naar deze sport',
          'filter_items_list' => 'Filter sportaanbod lijst',
          'filter_by_date' => 'Filter sportaanbod op datum',
          'items_list_navigation' => 'Sportaanbod lijst navigatie',
          'items_list' => 'Sportaanbod lijst',
          'item_published' => 'Sport gepubliceerd.',
          'item_published_privately' => 'Sport privé gepubliceerd.',
          'item_reverted_to_draft' => 'Sport teruggezet naar het concept.',
          'item_scheduled' => 'Sport gepland.',
          'item_updated' => 'Sport geüpdatet.',
          'item_link' => 'Sport link',
          'item_link_description' => 'Een link naar een sport.',
        ],
        'public' => true,
        'show_in_rest' => true,
        'supports' => [
          0 => 'title',
          1 => 'thumbnail',
        ],
        'delete_with_user' => false,
      ]
    );

    if ($result instanceof WP_Error) {
      return false;
    }

    return true;
  }
}
