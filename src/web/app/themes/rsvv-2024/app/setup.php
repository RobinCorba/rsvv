<?php

/**
 * Theme setup.
 */

namespace App;

use App\Blocks\CardBlock;
use App\Blocks\NewsCardsBlock;
use App\Blocks\OpenStreetMapBlock;
use App\Blocks\ScheduleBlock\index as ScheduleBlock;
use App\BlockStyles\ButtonBlockStyle;
use App\BlockStyles\GroupBlockStyle;
use App\BlockStyles\HeadingBlockStyle;
use App\BlockStyles\ParagraphBlockStyle;
use App\PostTypes\Activity;
use App\PostTypes\Location;
use App\PostTypes\Sport;
use App\PostTypes\TargetAudience;
use App\PostTypes\Trainer;
use App\Settings\ThemeSettings;

use function Roots\bundle;

// Load localization translations (see README.md for more information)
load_theme_textdomain('rsvv');

/**
 * Register the theme assets.
 *
 * @return void
 */
add_action('wp_enqueue_scripts', function () {
  bundle('app')->enqueue();
}, 100);

/**
 * Register the theme assets with the block editor.
 *
 * @return void
 */
add_action('enqueue_block_editor_assets', function () {
  bundle('editor')->enqueue();
}, 100);

/**
 * Register the initial theme setup.
 *
 * @return void
 */
add_action('after_setup_theme', function () {
  /**
   * Disable full-site editing support.
   *
   * @link https://wptavern.com/gutenberg-10-5-embeds-pdfs-adds-verse-block-color-options-and-introduces-new-patterns
   */
  remove_theme_support('block-templates');

  /**
   * Register the navigation menus.
   *
   * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
   */
  register_nav_menus([
    'primary_nav_dutch' => __('Primary Navigation (Dutch)', 'rsvv'),
    'primary_nav_english' => __('Primary Navigation (English)', 'rsvv'),
    'footer_nav_dutch' => __('Footer Navigation (Dutch)', 'rsvv'),
    'footer_nav_english' => __('Footer Navigation (English)', 'rsvv'),
  ]);

  /**
   * Disable the default block patterns.
   *
   * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#disabling-the-default-block-patterns
   */
  remove_theme_support('core-block-patterns');

  /**
   * Enable plugins to manage the document title.
   *
   * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
   */
  add_theme_support('title-tag');

  /**
   * Enable post thumbnail support.
   *
   * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
   */
  add_theme_support('post-thumbnails');

  /**
   * Enable responsive embed support.
   *
   * @link https://developer.wordpress.org/block-editor/how-to-guides/themes/theme-support/#responsive-embedded-content
   */
  add_theme_support('responsive-embeds');

  /**
   * Enable HTML5 markup support.
   *
   * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
   */
  add_theme_support('html5', [
    'caption',
    'comment-form',
    'comment-list',
    'gallery',
    'search-form',
    'script',
    'style',
  ]);

  /**
   * Enable selective refresh for widgets in customizer.
   *
   * @link https://developer.wordpress.org/reference/functions/add_theme_support/#customize-selective-refresh-widgets
   */
  add_theme_support('customize-selective-refresh-widgets');
}, 20);

/**
 * Register the theme sidebars.
 *
 * @return void
 */
add_action('widgets_init', function () {
  $config = [
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget' => '</section>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  ];
});

/**
 * Register custom post types and taxonomies
 */
add_action('init', function (): void {
  // Custom post types
  Activity::register();
  Location::register();
  Sport::register();
  TargetAudience::register();
  Trainer::register();

  // Custom Blocks
  CardBlock::register();
  NewsCardsBlock::register();
  OpenStreetMapBlock::register();
  ScheduleBlock::register();

  // Block styles
  ButtonBlockStyle::register();
  GroupBlockStyle::register();
  ParagraphBlockStyle::register();
  HeadingBlockStyle::register();

  // Theme settings
  ThemeSettings::register();
});


// Customize the title placeholder of custom post types
add_filter('enter_title_here', function ($default, $post): string {
  switch ($post->post_type) {
    case 'activity':
      return 'Naam activiteit toevoegen';
    case 'locatie':
      return 'Naam locatie toevoegen';
    case 'sport':
      return 'Naam van de sport';
    case 'target-audience':
      return 'Doelgroepnaam';
    case 'trainer':
      return 'Naam van trainer';
  }
  return $default;
}, 10, 2);
