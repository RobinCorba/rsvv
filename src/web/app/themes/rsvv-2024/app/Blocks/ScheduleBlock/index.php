<?php

namespace App\Blocks\ScheduleBlock;

use App\Models\Activity;

class index
{

  /**
   * Registers a custom block to the Gutenberg Editor
   *
   * @return bool Returns true if block is registered
   */
  public static function register(): bool
  {
    if (!function_exists('register_block_type') || !function_exists('acf_add_local_field_group')) {
      return false;
    }

    register_block_type(__DIR__ . '/block.json');

    acf_add_local_field_group([
      'key' => 'group_schedule_fields',
      'title' => 'Schedule block fields',
      'fields' => [
        [
          'key' => 'field_schedule_down_dates',
          'label' => 'Down dates',
          'name' => 'schedule__down_dates',
          'aria-label' => '',
          'type' => 'textarea',
          'instructions' => 'Note which dates the schedule does not apply',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'maxlength' => '',
          'rows' => '',
          'placeholder' => '',
          'new_lines' => '',
        ],
        [
          'key' => 'field_schedule_notes',
          'label' => 'Notes',
          'name' => 'schedule_notes',
          'aria-label' => '',
          'type' => 'textarea',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'maxlength' => '',
          'rows' => '',
          'placeholder' => '',
          'new_lines' => '',
        ],
      ],
      'location' => [
        [
          [
            'param' => 'block',
            'operator' => '==',
            'value' => 'rsvv/schedule',
          ],
        ],
      ],
      'menu_order' => 0,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
      'active' => true,
      'description' => '',
      'show_in_rest' => 0,
    ]);

    return true;
  }

  /**
   * Sorts activities by day and returns organized schedule
   *
   * @param array $activities Array of Activity objects
   * @return array Sorted activities grouped by day
   */
  public static function sort_activities_by_day(array $activities): array
  {
    $sorted_schedule = [];

    // Sort by day and then by time if available
    usort($activities, function ($a, $b) {
      // First compare days
      $day_compare = $a->day <=> $b->day;

      if ($day_compare === 0 && isset($a->time_start) && isset($b->time_start)) {
        // If same day and time exists, sort by time
        return $a->time_start <=> $b->time_start;
      }

      return $day_compare;
    });

    // Group activities by day
    foreach ($activities as $activity) {
      if (!isset($sorted_schedule[$activity->day])) {
        $sorted_schedule[$activity->day] = [];
      }
      $sorted_schedule[$activity->day][] = $activity;
    }

    return $sorted_schedule;
  }

  /**
   * Retrieves all available activities
   *
   * @return array Array with activities
   */
  public static function get_schedule(): array
  {
    $schedule = [];
    foreach (get_posts(['post_type' => 'activity']) as $activity) {
      array_push($schedule, new Activity($activity));
    }

    // return $schedule;
    return self::sort_activities_by_day($schedule);
  }

  /**
   * Returns the localized name of the day
   *
   * @param string $day_key
   *
   * @return string Name of the day
   */
  public static function get_localized_day(string $day_key): string
  {
    $days = [
      '1monday' => __('Monday', 'rsvv'),
      '2tuesday' => __('Tuesday', 'rsvv'),
      '3wednesday' => __('Wednesday', 'rsvv'),
      '4thursday' => __('Thursday', 'rsvv'),
      '5friday' => __('Friday', 'rsvv'),
      '6saturday' => __('Saturday', 'rsvv'),
      '7sunday' => __('Sunday', 'rsvv'),
    ];
    return $days[$day_key] ?? 'Unknown';
  }
}
