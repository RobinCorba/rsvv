<?php

namespace App\Blocks;

class OpenStreetMapBlock
{

  /**
   * Registers a custom block to the Gutenberg Editor
   *
   * @return bool Returns true if block is registered
   */
  public static function register(): bool
  {
    if (!function_exists('register_block_type') || !function_exists('acf_add_local_field_group')) {
      return false;
    }

    register_block_type(__DIR__ . '/OpenStreetMapBlock/block.json');

    acf_add_local_field_group(
      array(
        'key' => 'group_osm-block',
        'title' => 'osm-block',
        'fields' => array(
          array(
            'key' => 'field_osm-coordinate-lat',
            'label' => 'Latitude (Y-axis)',
            'name' => 'osm-coordinate-lat',
            'aria-label' => '',
            'type' => 'text',
            'instructions' => 'e.g. (51.955124)',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '',
              'class' => '',
              'id' => '',
            ),
            'default_value' => '',
            'maxlength' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
          ),
          array(
            'key' => 'field_osm-coordinate-long',
            'label' => 'Longitude (X-axis)',
            'name' => 'osm-coordinate-long',
            'aria-label' => '',
            'type' => 'text',
            'instructions' => 'e.g. (5.898315)',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '',
              'class' => '',
              'id' => '',
            ),
            'default_value' => '',
            'maxlength' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
          ),
          // array(
          //   'key' => 'field_osm-block-image',
          //   'label' => 'Image',
          //   'name' => 'osm-block-image',
          //   'aria-label' => '',
          //   'type' => 'image',
          //   'instructions' => '',
          //   'required' => 0,
          //   'conditional_logic' => 0,
          //   'wrapper' => array(
          //     'width' => '',
          //     'class' => '',
          //     'id' => '',
          //   ),
          //   'return_format' => 'array',
          //   'library' => 'all',
          //   'min_width' => '',
          //   'min_height' => '',
          //   'min_size' => '',
          //   'max_width' => '',
          //   'max_height' => '',
          //   'max_size' => '',
          //   'mime_types' => '',
          //   'preview_size' => 'medium',
          // ),
        ),
        'location' => array(
          array(
            array(
              'param' => 'block',
              'operator' => '==',
              'value' => 'rsvv/openstreetmap',
            ),
          ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
        'show_in_rest' => 0,
      ));

    return true;
  }
}
