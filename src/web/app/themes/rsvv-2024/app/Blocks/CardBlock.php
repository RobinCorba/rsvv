<?php

namespace App\Blocks;

class CardBlock
{

  /**
   * Registers a custom block to the Gutenberg Editor
   *
   * @return bool Returns true if block is registered
   */
  public static function register(): bool
  {
    if (!function_exists('register_block_type') || !function_exists('acf_add_local_field_group')) {
      return false;
    }

    register_block_type(__DIR__ . '/CardBlock/block.json');

    acf_add_local_field_group(
      array(
        'key' => 'group_rsvv-card-block',
        'title' => 'rsvv-card-block',
        'fields' => array(
          array(
            'key' => 'field_rsvv-card-block-name',
            'label' => 'Name',
            'name' => 'rsvv-card-block-name',
            'aria-label' => '',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '',
              'class' => '',
              'id' => '',
            ),
            'default_value' => '',
            'maxlength' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
          ),
          array(
            'key' => 'field_rsvv-card-block-image',
            'label' => 'Image',
            'name' => 'rsvv-card-block-image',
            'aria-label' => '',
            'type' => 'image',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '',
              'class' => '',
              'id' => '',
            ),
            'return_format' => 'array',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
            'preview_size' => 'medium',
          ),
          array(
            'key' => 'field_rsvv-card-block-url',
            'label' => 'Link',
            'name' => 'rsvv-card-block-url',
            'aria-label' => '',
            'type' => 'url',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '',
              'class' => '',
              'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
          ),
        ),
        'location' => array(
          array(
            array(
              'param' => 'block',
              'operator' => '==',
              'value' => 'rsvv/card',
            ),
          ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
        'show_in_rest' => 0,
      ));

    return true;
  }
}
