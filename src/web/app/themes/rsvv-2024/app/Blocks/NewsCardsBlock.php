<?php

namespace App\Blocks;

class NewsCardsBlock
{

  /**
   * Registers a custom block to the Gutenberg Editor
   *
   * @return bool Returns true if block is registered
   */
  public static function register(): bool
  {
    if (!function_exists('register_block_type') || !function_exists('acf_add_local_field_group')) {
      return false;
    }

    register_block_type(__DIR__ . '/NewsCardsBlock/block.json');

    return true;
  }
}
