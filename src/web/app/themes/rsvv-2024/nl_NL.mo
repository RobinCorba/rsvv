��            )   �      �     �     �     �     �     �     �               ;     G     V     _     h     o     �     �     �  	   �  k   �  :     n   X     �     �     �               )     ?  c  [     �     �     �     �               3     J  	   ]     g     y     �     �     �     �  	   �     �     �  ]   �  1   8  k   j     �     �     �     �     �  
   �     	                                           
                                                                	                               Footer Navigation (Dutch) Footer Navigation (English) Friday Full Width Background Monday Open primary navigation Primary Navigation (Dutch) Primary Navigation (English) RSVV Button RSVV Underline Saturday Schedule Sunday The map is broken The map is loading.. Thursday Tuesday Wednesday block descriptionA block displaying a geographical location on OpenStreetMap with the OpenLayers interface block descriptionA card with a hyperlink, image and text. block descriptionAn block of cards showing the latest posts with a title, date, excerpt, image and hyperlink. block keywordcard block keywordlocation block keywordmap block keywordnews block keywordtile block titleRSVV Card block titleRSVV News Cards Project-Id-Version: RSVV 2024 0.1.9
Report-Msgid-Bugs-To: https://wordpress.org/support/theme/rsvv-2024
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
X-Generator: WP-CLI 2.11.0
X-Domain: rsvv
 Footernavigatie (Nederlands) Footernavigatie (Engels) Vrijdag Achtergrond Volle Breedte Maandag Open hoofdnavigatie Hoofdmenu (Nederlands) Hoofdmenu (Engels) RSVV Knop RSVV Onderstreept Zaterdag Rooster Zondag De kaart is kapot De kaart is aan het laden.. Donderdag Dinsdag Woensdag Een block dat een geograpische locatie toont op een OpenStreetMap met de Openlayers interface Een kaart met een hyperlink, afbeelding en tekst. Een block met kaarten van de laatste berichten met een titel, datum, samenvatting, afbeelding en hyperlink. kaart locatie kaart nieuws tegel RSVV Kaart RSVV Nieuwskaarten 