export default class CollapsableMenu {
  #menuButtonID = 'menu-button';
  #menuButtonClass = 'header-nav__menu-button';
  #menuID = 'header-nav__menu';
  #menuButtonElement;
  #menuElement;

  constructor() {
    // The menu button
    this.#menuButtonElement = document.querySelector(`#${this.#menuButtonID}`);
    if (!this.#menuButtonElement) {
      console.warn(`Failed initializing CollapsableMenu; missing element #${this.#menuButtonID}`);
      return;
    }

    // The menu itself
    this.#menuElement = document.querySelector(`#${this.#menuID}`);
    if (!this.#menuElement) {
      console.warn(`Failed initializing CollapsableMenu; missing element #${this.#menuID}`);
      return;
    }
    this.#menuButtonElement.addEventListener('click', this.toggleMobileMenu.bind(this));

    // Make all parent menu items clickable
    this.parentElements = document.querySelectorAll('.menu-item-has-children > a');
    this.parentElements.forEach((parentElement) => {
      parentElement.addEventListener('click', this.toggleSubmenu.bind(this));
    });
  }

  toggleMobileMenu() {
    this.#menuButtonElement.classList.toggle(`${this.#menuButtonClass}--opened`);
    this.#menuElement.classList.toggle(`${this.#menuID}--visible`);
  }

  toggleSubmenu(event) {
    event.preventDefault();
    const { parentElement } = event.target.parentElement;
    parentElement.classList.toggle('menu-item--opened');
    const submenu = parentElement.querySelector('.sub-menu');
    submenu.classList.toggle('sub-menu--visible');
  }
}
