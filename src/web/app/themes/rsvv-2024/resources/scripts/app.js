import domReady from '@roots/sage/client/dom-ready'; // eslint-disable-line import/no-unresolved
import CollapsableMenu from './menu';

/**
 * Application entrypoint
 */
domReady(async () => {
  new CollapsableMenu();
});

/**
 * @see {@link https://webpack.js.org/api/hot-module-replacement/}
 */
if (import.meta.webpackHot) {
  import.meta.webpackHot.accept(console.error); // eslint-disable-line no-console
}
