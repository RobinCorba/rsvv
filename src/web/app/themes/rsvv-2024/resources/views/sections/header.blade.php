<header class="header">
  <div class="header-nav">
    <div class="header-nav__content">
      <a class="header-nav__site" href="{{ home_url('/') }}">{!! $site_name !!}</a>
      <button id="menu-button" class="header-nav__menu-button" aria-label="{{ __('Open primary navigation', 'rsvv') }}"></button>
      <div id="header-nav__menu" class="header-nav__menu">

        <div class="menu__language-switcher">
          <a class="language-switcher__link language-switcher__link--active" aria-label="{{ __('Switch to Dutch') }}" href="/" lang="nl" hreflang="nl">NL</a>
          <a class="language-switcher__link" aria-label="{{ __('Switch to English') }}" href="/en" lang="en" hreflang="en">EN</a>
        </div>

        @if (has_nav_menu('primary_nav_dutch'))
          <nav class="menu__primary" aria-label="{{ wp_get_nav_menu_name('primary_nav_dutch') }}">
            {!! wp_nav_menu([
                'theme_location' => 'primary_nav_dutch',
                'menu_class' => 'nav',
                'echo' => false,
            ]) !!}
          </nav>
        @endif

      </div>
    </div>
  </div>
  <?php
    $page_thumbnail_url = get_the_post_thumbnail_url();
    if (!empty($page_thumbnail_url)) :
  ?>
  <div class="header-page-hero" style="background-image: url('{!! $page_thumbnail_url !!}')"></div>
  <?php endif; ?>
</header>
