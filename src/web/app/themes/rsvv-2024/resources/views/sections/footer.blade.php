@php
  $slogan = get_field('footer_slogan', 'option');
  $facebook_url = get_field('socmed_facebook_url', 'option');
  $instagram_url = get_field('socmed_instagram_url', 'option');
  $address = get_field('contact_address', 'option');
  $phone = get_field('contact_phone_number', 'option');
  $phone_note = get_field('contact_phone_number_note', 'option');
  $email = get_field('contact_email', 'option');

  // Remove a leading zero
  $phone_href = preg_replace("/\(0\)/", "", $phone);
  // Replace plus sign with double zero
  $phone_href = preg_replace("/\+/", "00", $phone_href);
  // Remove any non digit
  $phone_href = preg_replace("/[^0-9]/", "", $phone_href);
@endphp

<footer class="footer">

  <div class="footer__hero">
    <div class="footer__signup">
      <img class="footer__signup-logo" src="@asset('../images/rsvv-logo.png')" alt="" />
      <div class="footer__signup-cta">
        <div>
          <h2>Vragen of wil je inschrijven?</h2>
          <p>Wanneer u het contactformulier of inschrijvingsformulier invult, nemen wij vervolgens zo spoedig mogelijk
            contact met u op. RSVV helpt u graag verder met een uitgebreid sportaanbod.</p>
        </div>
      </div>
      <div class="footer__signup-button is-style-rsvv-button">
        <a href="#">Neem contact op</a>
      </div>

    </div>
  </div>

  <div class="footer__menu-container">
    <div class="footer__menu">

      <div class="footer__menu-item">
        <div class="footer__header">{!! $site_name !!}</div>
        @if (!empty($slogan))
          <div class="footer__header-slogan"><?= get_field('footer_slogan', 'option') ?></div>
        @endif
        <div class="footer__menu--socials">
          @if (!empty($facebook_url))
            <a class="social-icon social-icon--facebook" href="<?= $facebook_url ?>" target="new">RSVV Facebook</a>
          @endif
          @if (!empty($instagram_url))
            <a class="social-icon social-icon--instagram" href="<?= $instagram_url ?>" target="new">RSVV Instagram</a>
          @endif
        </div>
      </div>

      <div class="footer__menu-item footer__menu--nav">
        <div class="footer__header">Handige links</div>
        <div>
          @if (has_nav_menu('footer_nav_dutch'))
            <nav class="menu__footer" aria-label="{{ wp_get_nav_menu_name('footer_nav_dutch') }}">
              {!! wp_nav_menu([
                  'theme_location' => 'footer_nav_dutch',
                  'menu_class' => 'nav',
                  'echo' => false,
              ]) !!}
            </nav>
          @endif
        </div>
      </div>

      <div class="footer__menu-item footer__menu--contact">
        <div class="footer__header">Contact</div>
        <div class="contact-details">
          @if (!empty($address))
          <div class="contact-details__item contact-details__item--address">
              {!! $address !!}
          </div>
          @endif

          @if (!empty($phone))
          <div class="contact-details__item contact-details__item--phone">
            <div>
              <a href="tel:{!! $phone_href !!}">{!! $phone !!}</a>
              @if (!empty($phone_note))
              <em>{!! $phone_note !!}</em>
              @endif
            </div>
          </div>
          @endif

          @if (!empty($email))
          <div class="contact-details__item contact-details__item--email">
            <a href="mailto:{!! $email !!}">{!! $email !!}</a>
          </div>
          @endif
        </div>
      </div>

    </div>
  </div>

</footer>
