<?php $latest_posts = get_posts(['numberposts' => 3]); ?>
<ul <?= get_block_wrapper_attributes() ?>>
  <?php foreach($latest_posts as $post) :
  $thumbnail_url = get_the_post_thumbnail_url($post, 'medium_large');

  if (!$thumbnail_url) {
    $thumbnail_url = '/app/themes/rsvv-2024/public/images/rsvv-news-thumbnail.png';
  }
  ?>
  <li>
    <a <? if (!is_admin()) echo "href='".get_the_permalink($post)."'"; ?> class="news-card">
      <img class="news-card__image" src="<?= $thumbnail_url ?>" alt="">
      <span class="news-card__title"><?= $post->post_title ?></span>
      <span class="news-card__date"><?= $post->post_date ?></span>
      <span class="news-card__excerpt"><?= $post->post_excerpt ?></span>
      <span class="news-card__cta"><?= get_field('card_cta') ?: "Lees meer" ?></span>
    </a>
  </li>
  <?php endforeach; ?>
</ul>
