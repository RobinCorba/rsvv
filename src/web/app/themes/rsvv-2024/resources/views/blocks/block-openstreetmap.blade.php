<?
// Generate a unique code in case this block is used multiple times
$unique_map_code = substr(md5(rand()), 0, 5);

// Get coordinates of block
$lat = get_field('osm-coordinate-lat');
$long = get_field('osm-coordinate-long');
?>

<div <?= get_block_wrapper_attributes() ?>>
  <div class="wp-block-rsvv-openstreetmap__loading" id="block-osm-loader-<?= $unique_map_code ?>">
    <? if (empty($lat) || empty($long)) : ?>
    <span><?= __('The map is broken', 'rsvv') ?></span>
    <? else : ?>
    <span><?= __('The map is loading..', 'rsvv') ?></span>
    <? endif; ?>
  </div>
  <div class="wp-block-rsvv-openstreetmap__container" id="block-osm-<?= $unique_map_code ?>"></div>
</div>

<? if (!empty($lat) && !empty($long)) : ?>
<script src="//openlayers.org/api/OpenLayers.js" type="text/javascript"></script>
<script>
  function init_osm() {
    window.setTimeout(() => {
      // Render OpenStreetMap
      map = new OpenLayers.Map('block-osm-<?= $unique_map_code ?>');
      originalOSM = new OpenLayers.Layer.OSM("OpenStreetMap");
      map.addLayers([originalOSM]);

      var lonLat = new OpenLayers.LonLat(<?= $long ?>, <?= $lat ?>)
        .transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());

      var zoom = 17;

      // Define where to center on
      map.setCenter(lonLat, zoom);

      // Add map marker
      var markers = new OpenLayers.Layer.Markers("Markers");
      map.addLayer(markers);
      markers.addMarker(new OpenLayers.Marker(lonLat));
    }, 50); // The delay is necessary to solve a bug where the map isn't fully loaded

    // Show the map
    window.setTimeout(() => {
      document.querySelector('#block-osm-loader-<?= $unique_map_code ?>')
        .classList.add('wp-block-rsvv-openstreetmap--hidden');
    }, 800);
  }
  window.onload = init_osm();
</script>
<? endif; ?>
