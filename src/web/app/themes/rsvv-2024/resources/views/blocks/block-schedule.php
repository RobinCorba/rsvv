<?php

use App\Blocks\ScheduleBlock\index as ScheduleBlock;

$schedule = ScheduleBlock::get_schedule();
?>
<div class="schedule">
  <div class="schedule-container">
    <?php foreach ($schedule as $day => $activities) : ?>
      <div class="schedule-day" data-day="<?= preg_replace('/[^0-9]/', '', $day) ?>">
        <h2 class="schedule-title"><?= ScheduleBlock::get_localized_day($day) ?></h2>
        <div class="schedule-table-container">
          <table class="schedule-table">
            <thead>
              <tr class="schedule-table__head">
                <th class="schedule-table__head-time">Tijd</th>
                <th class="schedule-table__head-activity">Activiteit</th>
                <th class="schedule-table__head-audience">Doelgroep</th>
                <th class="schedule-table__head-trainer">Trainer</th>
                <th class="schedule-table__head-location">Locatie</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($activities as $activity) : ?>
                <tr class="schedule-table__activity">
                  <td class="schedule-table__activity-time"><?= substr($activity->time_start, 0, -3) ?> - <?= substr($activity->time_end, 0, -3) ?></td>
                  <td class="schedule-table__activity-activity"><?= $activity->sport->name ?></td>
                  <td class="schedule-table__activity-audience"><?= $activity->audience->name ?></td>
                  <td class="schedule-table__activity-trainer"><?= $activity->trainer->name ?></td>
                  <td class="schedule-table__activity-location"><span class="activity-label">Locatie: </span><?= $activity->location->name ?></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
  <div class="schedule-notes">
    <?=
      get_field('schedule_notes');
    ?>
  </div>
</div>

<script type="text/javascript">
  window.onload = renderSchedule;

  function toggleScheduleDay(element) {
    element.classList.toggle('schedule-day--opened');
  }

  function renderSchedule() {
    // Add click listeners to each day
    const days = document.querySelectorAll('.schedule-day');
    days.forEach((element, index) => {
      element.addEventListener("click", (element) => { toggleScheduleDay(element.target.parentNode) });

      // Open the current day by default
      const today = new Date().getDay();
      if (element.dataset.day == today) {
        toggleScheduleDay(element);
      }
    });
  }
</script>
