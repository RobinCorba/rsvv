<?php $block_url = get_field('rsvv-card-block-url'); ?>
<a <?= get_block_wrapper_attributes() ?> <? if (!is_admin()) echo "href='{$block_url}'"; ?>>
  <img class="wp-block-rsvv-card__image" src="<?= get_field('rsvv-card-block-image')["sizes"]["medium_large"] ?>" alt="">
  <span class="wp-block-rsvv-card__name"><?= get_field('rsvv-card-block-name') ?></span>
</a>
